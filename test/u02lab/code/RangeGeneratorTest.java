package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RangeGeneratorTest {

    private static int START = 0;
    private static int STOP = 100;
    final int ELEMENT_TO_BE_CONSUMED = 13;
    private SequenceGenerator rangeGenerator;

    @Before
    public void Initialize(){
        rangeGenerator = new RangeGenerator(START,STOP);
    }

    @Test
    public void next() {
        for (int i=START+1; i<STOP; i++) {
            assertEquals(Optional.of(i), rangeGenerator.next());
        }
    }

    @Test
    public void reset() {
        rangeGenerator.reset();
        assertEquals(Optional.of(START+1), rangeGenerator.next());
        rangeGenerator = new RangeGenerator(START,STOP);
        elementToConsume();
        rangeGenerator.reset();
        assertEquals(Optional.of(START+1), rangeGenerator.next());
    }

    @Test
    public void isOver() {
        assertTrue(rangeGenerator.isOver());
    }

    @Test
    public void allRemaining() {

        int actual_next = 0;
        List<Integer> remainingTestList = new ArrayList<>();
        for (int i = START + ELEMENT_TO_BE_CONSUMED +1; i<STOP; i++) {
            remainingTestList.add(i);
        }
        elementToConsume();
        List<Integer> remainingMethodList = new ArrayList<>(rangeGenerator.allRemaining());

        assertEquals(remainingTestList,remainingMethodList);
    }

    private void elementToConsume(){
        for (int i = START ; i<ELEMENT_TO_BE_CONSUMED; i++) {
            rangeGenerator.next();
        }
    }
}