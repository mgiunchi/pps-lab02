package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private int actual;
    private int stop;
    private int start;

    public RangeGenerator(int start, int stop){
        this.actual = start;
        this.stop = stop;
        this.start = start;
    }

    @Override
    public Optional<Integer> next() {
        if (this.actual<this.stop){
            this.actual++;
            return Optional.of(actual);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        this.actual = start;
    }

    @Override
    public boolean isOver() {
        return this.actual<this.stop;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> listRemaining = new ArrayList<>();
        for (int value=this.actual+1;value<this.stop;value++){
            listRemaining.add(value);
        }
        return listRemaining;
    }
}
